
from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("profile/<str:profile_name>", views.profile, name="profile"),
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("register", views.register, name="register"),
    path("following", views.following, name="following"),

    # API Routes
    path("follow/<str:profile_user>", views.follow, name="follow"),
    path("post/<int:post_id>", views.post, name="post"),
    path("like/<int:post_id>", views.like, name="like"),

]
