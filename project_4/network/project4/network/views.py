from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.db import IntegrityError
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

from .models import User, Post, Follower, Like
import json


def index(request):
    if request.method == "POST":
        post_content = request.POST["content"].strip()
        try:
            user_id = request.session["user_id"]
        except:
            user_id = None
        
        if user_id:
            if post_content:
                Post.objects.create(user_id=user_id, content=post_content)
                return HttpResponseRedirect(reverse("index"))
            else:
                message = ' Invalid post (empty).'
    else:
        message = ''
    
    posts = Post.objects.all().order_by('-timestamp')
    page_number = request.GET.get('page')
    page_obj = get_page_obj(posts,page_number)
    
    return render(request, "network/index.html", {
        "title": "All Posts",
        "message": message,
        "page_obj": page_obj
        } )

@csrf_exempt
@login_required
def following(request):
    
    user_id = request.session["user_id"]

    followed_users = Follower.objects.filter(following_id=user_id)
    following_list = [user.followed_id for user in followed_users]
    posts = Post.objects.filter(user__in=following_list).order_by('-timestamp')
    page_number = request.GET.get('page')
    page_obj = get_page_obj(posts,page_number)

    return render(request, "network/index.html", {
        "title": "Following",
        "page_obj": page_obj
        } )

def profile(request,profile_name):
    
    try:
        uid = User.objects.get(username=profile_name).id
        title = profile_name
        profile_msg = get_profile_msg(uid)
        profile_err= None
    except:
        uid = None
        title = 'Invalid username'
        profile_msg = ''
        profile_err = 'Please enter a valid username.'
        
    if uid:
        posts = Post.objects.filter(user=uid).order_by('-timestamp')
    else:
        posts = None
    
    page_number = request.GET.get('page')
    page_obj = get_page_obj(posts,page_number)
    
    return render(request, "network/index.html", {
        "title": title,
        "profile_err": profile_err,
        "page_obj": page_obj,
        "profile": True,
        "profile_msg": profile_msg
         } )


def login_view(request):
    if request.method == "POST":

        # Attempt to sign user in
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)

        # Check if authentication successful
        if user is not None:
            login(request, user)
            request.session["user_id"] = user.id
            return HttpResponseRedirect(reverse("index"))
        else:
            return render(request, "network/login.html", {
                "message": "Invalid username and/or password."
            })
    else:
        return render(request, "network/login.html")


def logout_view(request):
    logout(request)
    request.session["user_id"] = ''
    return HttpResponseRedirect(reverse("index"))


def register(request):
    if request.method == "POST":
        username = request.POST["username"]
        email = request.POST["email"]

        # Ensure password matches confirmation
        password = request.POST["password"]
        confirmation = request.POST["confirmation"]
        if password != confirmation:
            return render(request, "network/register.html", {
                "message": "Passwords must match."
            })

        # Attempt to create new user
        try:
            user = User.objects.create_user(username, email, password)
            user.save()
        except IntegrityError:
            return render(request, "network/register.html", {
                "message": "Username already taken."
            })
        login(request, user)
        request.session["user_id"] = user.id
        return HttpResponseRedirect(reverse("index"))
    else:
        return render(request, "network/register.html")


def get_profile_msg(profile_id):
    followed_number = len(Follower.objects.filter(followed_id = profile_id))
    following_number = len(Follower.objects.filter(following_id = profile_id))
        
    return f'{followed_number} Followers - {following_number} Following'

#API
@csrf_exempt
@login_required
def follow(request, profile_user):
    req_user_id = request.user.id
    profile_id = User.objects.get(username=profile_user).id
    if request.method == "GET":
        users_relation = len(Follower.objects.filter(following_id=req_user_id, followed_id = profile_id))
        if users_relation > 0:
            button_title = 'Unfollow'
        else:
            button_title = 'Follow'
    
    elif request.method == "PUT":
        data = json.loads(request.body)
        if data['action'] == 'Follow':
            following = Follower(following_id=req_user_id, followed_id = profile_id)
            following.save()
            # print('Usuário adicionado com sucesso')
            button_title = 'Unfollow'
        elif data['action'] == 'Unfollow':
            following = Follower.objects.get(following_id=req_user_id, followed_id = profile_id)
            following.delete()
            # print('Usuário removido com sucesso')
            button_title = 'Follow'
        else:
            return JsonResponse({"error": "data['action'] dever ser igual a 'Follow' ou 'Unfollow'."}, status=400)

    else:
        return JsonResponse({"error": "GET or PUT request required."}, status=400)

    profile_msg = get_profile_msg(profile_id)
    return JsonResponse({
            "button_title": button_title,
            "profile_msg": profile_msg
        })

#API
@csrf_exempt
@login_required
def post(request, post_id):

    if request.method != "PUT":
        return JsonResponse({
            "error": "PUT request required."
        }, status=400)
    
    edit_post = Post.objects.get(pk=post_id)
    
    post_user_id = edit_post.user.id
    req_user_id = request.user.id
    if req_user_id != post_user_id:
        return JsonResponse({
            "error": "a user can't edit other a post from other users."
        }, status=400)
    
    data = json.loads(request.body)
    edit_text = data['post_content']
    if edit_text is not None:
        edit_post.content = edit_text
        edit_post.save()
        
    return HttpResponse(status=204)

#API
@csrf_exempt
# @login_required
def like(request, post_id):

    req_user_id = request.user.id
    post_state = len(Like.objects.filter(post_id=post_id, user_id=req_user_id))

    if request.method == "GET":
        if req_user_id:
            if post_state > 1:
                response = {
                    "error": "Error! More than one like for the same user. Please check your database.",
                    "post_state": 1
                }
            else:
                response = { "post_state": post_state }
        else:
            response = { "post_state": -1 }

        likes_count = len(Like.objects.filter(post_id=post_id))
        response['likes_count'] = likes_count

        return JsonResponse(response)
    
    elif request.method == "PUT":
        if req_user_id:
            if post_state == 0:
                Like.objects.create(post_id=post_id, user_id=req_user_id)
            else:            
                likes = Like.objects.get(post_id=post_id, user_id=req_user_id)
                likes.delete()
            return HttpResponse(status=204)
        else:
            return JsonResponse({
                "error": "The user needs to log in to like any posts."
                }, status=400)      
    else:
        return JsonResponse({
            "error": "GET or PUT request required."
        }, status=400)
        
        

    
    
    

def get_page_obj(objects,page_number,objs_per_page=10):
    paginator = Paginator(objects, objs_per_page)
    return paginator.get_page(page_number)
