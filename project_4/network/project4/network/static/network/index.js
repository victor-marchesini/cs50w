
document.addEventListener('DOMContentLoaded', function() {

  follow_button_div = document.querySelector('#follow_button_div')
  if (follow_button_div) {
    add_button(follow_button_div);
  }

  var posts = document.getElementsByClassName('card post');
  for (let post of posts) {
    if (post.id) {
      var edit_button = post.getElementsByClassName('edit_button')[0];
      if (edit_button) {
        edit_button.addEventListener('click', () => edit_post(post) );
      }
      set_initial_like_state(post);
    }
  }
});

function edit_post(post) {
  post_contents = post.getElementsByClassName("post_content");

  if (post_contents.length == 2) {
    alert('Please click either on a buton below to "Save changes" or "Cancel" them.');
  }
  else {
    const content_div = post_contents[0]

    original_text = content_div.textContent.trim();

    post_input = '<div class="row mb-2"> <div class="col post_content"> <textarea rows="5" class="form-control">'
    post_input += original_text + '</textarea> </div></div>'
    content_div.innerHTML = post_input;

    const save_changes = document.createElement('button');
    save_changes.className = 'btn btn-primary mr-2';
    save_changes.textContent = 'Save changes';
    save_changes.addEventListener('click', () => update_post(content_div,post.id,original_text));
    content_div.appendChild(save_changes);

    const cancel_changes = document.createElement('button');
    cancel_changes.className = 'btn btn-outline-secondary';
    cancel_changes.textContent = 'Cancel';
    cancel_changes.addEventListener('click', () => {content_div.innerHTML = original_text;});
    content_div.appendChild(cancel_changes);
  }
  
}

function update_post(content_div,post_id,original_text) {
  edit_text = content_div.getElementsByTagName('textarea')[0].value.trim();
  if (original_text == edit_text) {
    var confirmation = false
  }
  else {
    var message = "Please confirm your change.\n\n"
    message += "Original content:\n<" + original_text + ">\n\n"
    message += "New content:\n<" + edit_text + ">\n"
    var confirmation = confirm(message);
  }

  if (confirmation == true) {
    if (edit_text.length > 0) {
      fetch(`/post/${post_id}`, {
        method: 'PUT',
        body: JSON.stringify({
          post_content: edit_text
        })
      })
      content_div.innerHTML = edit_text;
    }
    else {
      alert("Please enter a valid post!");
    }  
  }
  else {
    content_div.innerHTML = original_text
  }
}

function follow(username) {

  button_element = document.querySelector('#follow_button')

  fetch(`/follow/${username}`, {
    method: 'PUT',
    body: JSON.stringify({
        action: button_element.textContent
    })
  })
  .then(response => response.json())
  .then(results => {
    button_element.textContent = results['button_title'];
    document.querySelector('#profile_followers').textContent = results['profile_msg'];
  })
}

function add_button(follow_button_div) {
  username = document.querySelector('h1').textContent

  fetch(`/follow/${username}`)
  .then(response => response.json())
  .then(results => {

      const element = document.createElement('button');
      element.setAttribute("id", 'follow_button');
      element.className = 'btn btn-sm btn-outline-primary';
      
      element.textContent = results['button_title'];
      
      element.addEventListener('click', () => follow(username) );
      follow_button_div.append(element);
  })
}

function set_initial_like_state(post) {

  const like_button = post.getElementsByClassName('like_button')[0];
  like_button.addEventListener('click', () => like_post(like_button,post) );

  fetch(`/like/${post.id}`)
  .then(response => response.json())
  .then(results => {

    post_state = results["post_state"];

    if (post_state < 0){
      like_button.disabled = true; //keep button disabled
    }
    else {
      like_button.disabled = false;
      
      if (post_state == 0) {
        like_button.className = "btn btn-outline-danger btn-sm like_button";
        like_button.title = "Like"; 
      }
      else {
        like_button.className = "btn btn-danger btn-sm like_button";
        like_button.title = "Unlike";
        if (post_state > 1) {
          alert('More than one like for the same user. Please report to the database admin.');
        }
      }
    }

    likes_count = results["likes_count"];

    var likes_counter = post.querySelector('#likes_count');
    likes_counter.textContent = likes_count;
  });
}

function like_post(like_button,post) {
  
  var likes_counter = post.querySelector('#likes_count');
  var n_likes = parseInt(likes_counter.textContent);

  if (like_button.title == "Unlike") {
    like_button.className = "btn btn-outline-danger btn-sm like_button";
    like_button.title = "Like";
    likes_counter.textContent = n_likes - 1
  }
  else {
    like_button.className = "btn btn-danger btn-sm like_button";
    like_button.title = "Unlike";
    likes_counter.textContent = n_likes + 1
  }

  fetch(`/like/${post.id}`, {
    method: 'PUT'
  })
}