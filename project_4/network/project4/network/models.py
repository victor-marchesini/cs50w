from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):

    def __str__(self):
        return f"{self.username}"

class Post(models.Model):
    content = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_posts",default=1)
    timestamp = models.DateTimeField(auto_now_add=True)
    likes = models.IntegerField(default=0)

class Follower(models.Model):
    following_id = models.IntegerField()
    followed_id = models.IntegerField()

class Like(models.Model):
    post_id = models.IntegerField()
    user_id = models.IntegerField()
    