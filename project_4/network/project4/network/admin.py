from django.contrib import admin
from .models import Post, User, Follower, Like

# Register your models here.

class PostAdmin(admin.ModelAdmin):
    list_display = ("id", "content", "user", "timestamp", "likes")

class UserAdmin(admin.ModelAdmin):
    list_display = ("id", "username", "email")

class FollowerAdmin(admin.ModelAdmin):
    list_display = ("id", "following_id", "followed_id")

class LikeAdmin(admin.ModelAdmin):
    list_display = ("id", "post_id", "user_id")

admin.site.register(Post, PostAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(Follower, FollowerAdmin)
admin.site.register(Like, LikeAdmin)