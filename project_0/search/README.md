## Projec 0 - [Search](https://cs50.harvard.edu/web/2020/projects/0/search/)

- index.hmtl: Google Search
- image.html: Google Image Search
- advanced.hmtl: Google Advanced Search
- styles.css: css definitions for the 3 pages
- doodle.png - source: https://www.google.com/doodles/doodle-4-google-2017-us-winner

screencast url: https://youtu.be/aUnYBPOnyfI
- video thumbnail: video_thumbnail.png (not used in Search pages)