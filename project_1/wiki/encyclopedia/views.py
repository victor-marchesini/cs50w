from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render
import markdown2 
from random import choice

from . import util


def index(request):
    if request.method == "POST":
        return query(request)

    return render(request, "encyclopedia/index.html", {
        "entries": util.list_entries()
    })

def entry(request, title):
    if request.method == "POST":
        return query(request)
    
    entries_list = util.list_entries()

    if title == "random_page":
        entry = choice(entries_list)
        return HttpResponseRedirect(reverse("encyclopedia:entry", args=[entry]))

    elif title in entries_list:
        return render(request, "encyclopedia/entries.html", {
            "title": title,
            "entry": markdown2.markdown(util.get_entry(title)) 
            })
    else:
        return query(request,title)

def query(request,title=None):
    
    if not title:
        title = request.POST["q"]

    entries_list = util.list_entries()
    lower_entries = [x.lower() for x in entries_list]

    if title.lower() in lower_entries:
        index = lower_entries.index(title.lower())
        return HttpResponseRedirect(reverse("encyclopedia:entry", args=[entries_list[index]]))
    else:
        search_list = [ x for x in entries_list if title.lower() in x.lower()]
        return render(request, "encyclopedia/not_found.html", {
            "title": title,
            "search_list": search_list
            })

def new_page(request):
    if request.method == "POST":
        new_title = request.POST["wiki_title"]
        wiki_content = request.POST["wiki_content"]
        entries_list = util.list_entries()
        lower_entries = [x.lower() for x in entries_list]
        
        invalid_titles = ['new_page','random_page']
        if new_title in invalid_titles:
            return render(request, "encyclopedia/new_page.html",{
                "error_message": 'This is not a valid title.',
                "wiki_content": wiki_content,
                "wiki_title": new_title
            })
        elif new_title.lower() in lower_entries:
            index = lower_entries.index(new_title.lower())
            title = entries_list[index]
            error_message = 'There is a current page with this title.'
            sugestion_message = f'You can change the current title or edit the <a href={title}>{title}</a> page.'
            return render(request, "encyclopedia/new_page.html",{
                "error_message": error_message,
                "sugestion_message": sugestion_message,
                "wiki_content": wiki_content,
                "wiki_title": new_title
            })
        else:
            new_file = open(f"entries/{new_title}.md","w")
            new_file.write(wiki_content)
            new_file.close()
            return HttpResponseRedirect(reverse("encyclopedia:entry", args=[new_title]))
    else:
        return render(request, "encyclopedia/new_page.html")

def edit(request, title):
    if request.method == "POST":
        content = request.POST["wiki_content"]
        util.save_entry(title, content)
        return HttpResponseRedirect(reverse("encyclopedia:entry", args=[title]))

    return render(request, "encyclopedia/edit_page.html", {
        "title": title,
        "entry": util.get_entry(title)
        })