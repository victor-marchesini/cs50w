Sass (short for **syntactically awesome style sheets**) is a preprocessor scripting language that is interpreted or compiled into Cascading Style Sheets ([CSS](/wiki/CSS)). SassScript is the scripting language itself. 

## Features

### Variables

Sass allows variables to be defined. Variables begin with a _dollar sign ($)_. Variable assignment is done with a _colon (:)_.

SassScript supports four data types:

- Numbers (including units)
- Strings (with quotes or without)
- Colors (name, or names)
- Booleans

### Nesting
    
CSS does support logical nesting, but the code blocks themselves are not nested. Sass allows the nested code to be inserted within each other.  