# Generated by Django 3.1.2 on 2020-11-12 11:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auctions', '0003_auto_20201109_0937'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='whatchlist',
            field=models.TextField(null=True),
        ),
    ]
