from django.contrib import admin
from .models import Bid, Comment, Listing, User      

# Register your models here.
class BidAdmin(admin.ModelAdmin):
    list_display = ("id", "value", "listing", "user")

class CommentAdmin(admin.ModelAdmin):
    list_display = ("id", "value", "listing", "user")

class ListingAdmin(admin.ModelAdmin):
    list_display = ("id", "open", "user", "title", "description", "starting_bid", "image_url", "category")

class UserAdmin(admin.ModelAdmin):
    list_display = ("id", "username", "first_name", "last_name", "email", "whatchlist")

admin.site.register(Bid, BidAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Listing, ListingAdmin)
admin.site.register(User, UserAdmin)