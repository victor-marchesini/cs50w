from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError
from django.db.models import Max
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

import markdown2

from .forms import UserForm
from .models import User, Listing, Bid, Comment

def categories(request):

    listings = Listing.objects.filter(open=True)
    categories = list(listings.values_list('category', flat=True).distinct())
    categories.sort()

    return render(request, "auctions/categories.html", {
        "categories": categories
    })

def index(request, status='opened'):
    error_message = ''
    listings = None
    if status == 'whatchlist':
        user_id = request.session["user_id"]
        if user_id:
            user = User.objects.get(pk=user_id)
            whatchlist_str = user.whatchlist
            if whatchlist_str:
                whatchlist = whatchlist_str.split(',')
                listings = Listing.objects.filter(pk__in=whatchlist)
        else:
            error_message = 'login required'
    else:
        if status == 'closed':
            listing_open = False    
        else:
            listing_open = True
        listings = Listing.objects.filter(open=listing_open)
    
    try:
        category_index = int(status)
        categories = list(listings.values_list('category', flat=True).distinct())
        categories.sort()
        selected_category = categories[category_index]
        listings = listings.filter(category=selected_category)
        if selected_category == '':
            selected_category = '(No category)'
    except ValueError:
        selected_category = ''

    return render(request, "auctions/index.html", {
        "listings": listings,
        "status": status,
        "category": selected_category,
        "error_message": error_message
    })

def create_bid(list_id,user_id,bid_value,current_listing):
    Bid.objects.create(listing_id=list_id, user_id=user_id, value=bid_value)
    current_listing.current_bid = bid_value
    current_listing.current_bid_user_id = user_id
    current_listing.save()

def listing(request, list_id):

    try:
        current_listing = Listing.objects.get(pk=list_id)
    except Listing.DoesNotExist:
        # messsage: Listing not available
        return HttpResponseRedirect(reverse("index"))
    
    user_id = request.session["user_id"]
    list_id_str = str(list_id)
    whatchlist = []
    error_message = ''
    bids = Bid.objects.filter(listing=list_id)
    listing_bids = bids.count()
    comments = Comment.objects.filter(listing=list_id)

    if bids:
        current_bid = float(bids.aggregate(Max('value'))['value__max'])
    else:
        starting_bid = current_listing.starting_bid
        current_bid = starting_bid

    if user_id:
        user = User.objects.get(pk=user_id)
        if request.method == "POST":
            action = request.POST["action"]
            current_whatchlist = user.whatchlist
            if action == 'add':                
                if current_whatchlist:
                    whatchlist = current_whatchlist.split(',')
                    if list_id_str not in whatchlist:
                        whatchlist.append(list_id_str)
                        whatchlist.sort()
                else:
                    whatchlist = [list_id_str]
            elif action == 'remove':
                whatchlist = current_whatchlist.split(',')
                if list_id_str in whatchlist:
                    whatchlist.remove(list_id_str)
            elif action == 'place_bid':
                new_bid = float(request.POST["new_bid"])
                if bids:
                    if new_bid > current_bid:
                        create_bid(list_id,user_id,new_bid,current_listing)
                        listing_bids += 1
                        current_bid = new_bid
                    else:
                        error_message = f'Value must be grater than the current bid.'
                else:
                    if new_bid >= starting_bid:
                        create_bid(list_id,user_id,new_bid,current_listing)
                        listing_bids += 1
                        current_bid = new_bid
                    else:
                        error_message = f'Minimum bid: {starting_bid}'
            elif action == 'close_auction':
                current_listing.open = False
                current_listing.save()
            elif action == 'new_comment':
                comment_text = request.POST["comment_text"]
                Comment.objects.create(listing_id=list_id, user_id=user_id, value=comment_text)
            
            if action == 'add' or action == 'remove':
                user.whatchlist = ','.join(whatchlist)
                user.save()

        if user.whatchlist:
            whatchlist = user.whatchlist.split(',')

    return render(request, "auctions/listing.html", {
        "listing": current_listing,
        "list_id": list_id_str,
        "whatchlist": whatchlist,
        "comments": comments,
        "current_bid": current_bid,
        "listing_bids": listing_bids, 
        "error_message": error_message
    })

def login_view(request):
    if request.method == "POST":

        # Attempt to sign user in
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)

        # Check if authentication successful
        if user is not None:
            login(request, user)
            request.session["user_id"] = user.id
            return HttpResponseRedirect(reverse("index"))
        else:
            return render(request, "auctions/login.html", {
                "message": "Invalid username and/or password."
            })
    else:
        return render(request, "auctions/login.html")


def logout_view(request):
    logout(request)
    request.session["user_id"] = ''
    return HttpResponseRedirect(reverse("index"))

@login_required
def new_listing(request):
    if request.method == "POST":

        title = request.POST["title"]
        description = request.POST["description"]
        starting_bid = request.POST["starting_bid"]
        image_url = request.POST["image_url"]
        category = request.POST["category"]
        description = markdown2.markdown(description)

        listing = Listing(title=title, description=description, starting_bid = starting_bid, image_url=image_url, category=category)
        listing.user_id = request.session["user_id"]
        listing.save()
        return HttpResponseRedirect(reverse("index"))
        
    else:
        return render(request, "auctions/new_listing.html")

def register(request):
    if request.method == "POST":
        form = UserForm(request.POST)
        error_message = ''
        if form.is_valid():
            username = form.cleaned_data["username"]
            email = form.cleaned_data["email"]
            first_name = form.cleaned_data["first_name"]
            last_name = form.cleaned_data["last_name"]

            # Ensure password matches confirmation
            password = form.cleaned_data["password"]
            confirmation = request.POST["confirmation"]
            if password != confirmation:
                error_message = 'Passwords must match.'
            else:
                # Attempt to create new user
                try:
                    user = User.objects.create_user(username,  email, password)
                    user.first_name = first_name
                    user.last_name = last_name
                    user.save()
                    login(request, user)
                    return HttpResponseRedirect(reverse("index"))

                except IntegrityError:
                    error_message =  'Username already taken.'
        else:
            error_message = 'Invalid form.'

        # Only if an error ocuurs: 
        return render(request, "auctions/register.html", {
                "message": error_message,
                "form": form
            })
        
    else:
        return render(request, "auctions/register.html",{
            "form": UserForm
        })
