from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("categories", views.categories, name="categories"),
    path("<str:status>/", views.index, name="index"),
    path("listing/<int:list_id>/", views.listing, name="listing"),
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("new_listing", views.new_listing, name="new_listing"),
    path("register", views.register, name="register")
]
