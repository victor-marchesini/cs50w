from django import forms
from .models import User

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username','first_name','last_name','email','password')
        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control','required':'True','autofocus':'True'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control','required':'True'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control','required':'True'}),
            'email': forms.EmailInput(attrs={'class': 'form-control','required':'True'}),
            'password': forms.PasswordInput(attrs={'class': 'form-control','autocomplete': 'new-password'})
        }