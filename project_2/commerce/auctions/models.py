from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    whatchlist = models.TextField(blank=True,null=True)
    def __str__(self):
        return f"{self.first_name} {self.last_name} ({self.username})"

class Listing(models.Model):
    title = models.CharField(max_length=128)
    description = models.TextField()
    starting_bid = models.DecimalField(max_digits=11, decimal_places=2)
    image_url = models.URLField(max_length=400,blank=True)
    category = models.CharField(max_length=64,blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_listings",default=1)
    current_bid = models.DecimalField(max_digits=11, decimal_places=2,null=True,blank=True)
    current_bid_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_bid_listings",null=True,blank=True)
    open = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.id} - {self.title}"

class Bid(models.Model):
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE, related_name="listing_bids",default=1)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_bids",default=1)
    value = models.DecimalField(max_digits=11, decimal_places=2)

class Comment(models.Model):
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE, related_name="listing_comments",default=1)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_comments",default=1)
    value = models.TextField()
    def __str__(self):
        return f"<strong>{self.user}:</strong> {self.value}"
