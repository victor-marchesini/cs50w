from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, "hello/index.html")

def victor(request):
    return HttpResponse("Olá, Victor!!!!!")

def greet(request, name):
    return render(request, "hello/greet.html", {
        "cap_name": name.capitalize()
    })
