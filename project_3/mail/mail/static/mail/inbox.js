document.addEventListener('DOMContentLoaded', function() {

  // Use buttons to toggle between views
  document.querySelector('#inbox').addEventListener('click', () => load_mailbox('inbox'));
  document.querySelector('#sent').addEventListener('click', () => load_mailbox('sent'));
  document.querySelector('#archived').addEventListener('click', () => load_mailbox('archive'));
  document.querySelector('#compose').addEventListener('click', () => compose_email());
  document.querySelector('#compose-form').onsubmit = send_email;

  // By default, load the inbox
  load_mailbox('inbox');
});

function compose_email(email) {

  // Show compose view and hide other views
  document.querySelector('#emails-view').style.display = 'none';
  document.querySelector('#compose-view').style.display = 'block';

  if (typeof email === 'undefined') {
    // Clear out composition fields
    document.querySelector('#compose-recipients').value = '';
    document.querySelector('#compose-subject').value = '';
    document.querySelector('#compose-body').value = '';
  }
  else {
    document.querySelector('#compose-recipients').value = email.sender;
    var newSubject = email.subject;
    strFirstThree = newSubject.substring(0,3).toUpperCase()
    if (strFirstThree !== 'RE:') {
      newSubject = 'Re: '.concat(newSubject);
    }
    document.querySelector('#compose-subject').value = newSubject;
    str1 = `\n---\nOn ${email.timestamp} ${email.sender} wrote:\n\n`
    document.querySelector('#compose-body').value = str1.concat(email.body);

  }

  const x = document.querySelector('textarea'); 
  
  x.style.width = "100%"; 
  x.style.color = "black"; 
  x.style.padding = "15px"; 

  if (x.nextElementSibling.tagName !== 'HR'){
    const hr = document.createElement('hr');
    document.querySelector('textarea').after(hr);
  }
    
  document.querySelector('input[type="submit"]').value='send';

}

function load_mailbox(mailbox) {
  
  // Show the mailbox and hide other views
  document.querySelector('#emails-view').style.display = 'block';
  document.querySelector('#compose-view').style.display = 'none';

  // Show the mailbox name
  document.querySelector('#emails-view').innerHTML = `<h3>${mailbox.charAt(0).toUpperCase() + mailbox.slice(1)}</h3>`;

  const new_body = document.createElement('div');
  new_body.setAttribute("id", 'emails-view-body');
  document.querySelector('#emails-view').append(new_body);
      
  fetch(`/emails/${mailbox}`)
  .then(response => response.json())
  .then(emails => {
    // Print emails
    // console.log(emails);

    for (key in emails) {
      
      msg = emails[key]

      const element = document.createElement('div');
      element.setAttribute("id", `${msg.id}`);
      element.className = 'row'
      element.style.border = "1px solid black";

      if (msg.read === true) {
        element.style.backgroundColor = "#AAAAAA";
      }
      else {
        element.style.backgroundColor = "white";
        element.style.fontWeight = "bold";
      }

      const recipients = document.createElement('div');
      recipients.className = 'col-lg-3 col-sm-3'; 
      if (mailbox === 'sent') {
        recipients.textContent = msg.recipients;
      }
      else {
        recipients.textContent = msg.sender;
      }
      element.appendChild(recipients);
      
      const subject = document.createElement('div');
      subject.className = 'col-lg-6 col-sm-6';
      subject.textContent = msg.subject;
      recipients.after(subject);

      
      const timeStamp = document.createElement('div');
      timeStamp.className = 'col-lg-3 col-sm-3';
      timeStamp.textContent = msg.timestamp;
      timeStamp.style.textAlign = "right";
      subject.after(timeStamp);

      element.addEventListener('click', () => read_email(element.id,mailbox) );        
      document.querySelector('#emails-view-body').append(element);
      
    }
  });

}

function load_mailbox_delay(mailbox,delay) {
  setTimeout(function (){
    load_mailbox(mailbox);
    }, delay); // How long do you want the delay to be (in milliseconds)  
}

function send_email() {

  fetch('/emails', {
    method: 'POST',
    body: JSON.stringify({
        recipients: document.querySelector('#compose-recipients').value,
        subject: document.querySelector('#compose-subject').value,
        body: document.querySelector('#compose-body').value
    })
  })
  // .then(response => response.json())
  // .then(result => {
  //     // Print result
  //     // console.log(result);
  // })

  load_mailbox_delay('sent',200);
  return false;
}

function read_email(email_id,mailbox) {

  fetch(`/emails/${email_id}`, {
    method: 'PUT',
    body: JSON.stringify({
        read: true
    })
  })
  
  fetch(`/emails/${email_id}`)
  .then(response => response.json())
  .then(email => {
    // Print email
    // console.log(email);
    
    str2 = '<div class="row">'
    str2 = str2.concat('<div class="col-lg-10 col-sm-6"> <h3>',email.subject,'</h3> </div>');
    if (mailbox === 'inbox') {
      str2 = str2.concat('<div class="col-lg-2"> <button style="float: right;" class="btn btn-outline-secondary" id="archive">Archive</button> </div>');
    }
    else if (mailbox === 'archive') {
      str2 = str2.concat('<div class="col-lg-2"> <button style="float: right;" class="btn btn-sm btn-outline-secondary" id="unarchive">Unarchive</button> </div>');
    }
    str2 = str2.concat('</div>')

    str2 = str2.concat('<div class="row"> <div class="col-lg-12 col-sm-4"> <b> From: </b>',email.sender,'</div> </div>');
    str2 = str2.concat('<div class="row"> <div class="col-lg-12 col-sm-4"> <b> To: </b>',email.recipients,'</div> </div>');
    str2 = str2.concat('<div class="row"> <div class="col-lg-12 col-sm-4"> <b> Subject: </b>',email.subject,'</div> </div>');
    str2 = str2.concat('<div class="row"> <div class="col-lg-12 col-sm-4"> <b> Timestamp: </b>',email.timestamp,'</div> </div>');
    str2 = str2.concat('<hr></hr>')
    str2 = str2.concat('<div class="row"> <div class="col-lg-12 col-sm-8"> <textarea disabled readonly>',email.body,'</textarea> </div></div>');
    str2 = str2.concat('<hr></hr>')
    str2 = str2.concat('<div class="row"> <div class="col-lg-2"> <button style="float: left;" class="btn btn-primary" id="reply">Reply</button> </div> </div>');
    document.querySelector('#emails-view').innerHTML = str2

    const x = document.querySelector('textarea'); 
  
    x.style.width = "100%"; 
    x.style.color = "black";
    x.style.padding = "15px"; 


    if (mailbox === 'inbox') {
      document.querySelector('#archive').addEventListener('click', () => archive_email(email_id,true) );
    } else if (mailbox === 'archive') {
      document.querySelector('#unarchive').addEventListener('click', () => archive_email(email_id,false) );
    }
    document.querySelector('#reply').addEventListener('click', () => compose_email(email) );
    
  });

}

function archive_email(email_id,flag) {
  fetch(`/emails/${email_id}`, {
    method: 'PUT',
    body: JSON.stringify({
      archived: flag
    })
  })
  load_mailbox_delay('inbox',200)
}