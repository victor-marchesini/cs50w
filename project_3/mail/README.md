## Project 3 - [Mail](https://cs50.harvard.edu/web/2020/projects/3/mail)

screencast url: [cs50w project3 - Mail (2020)](https://youtu.be/4AheeZNX5nI)

setup:
python version: 3.8.10 (may work on different versions)

create a new environment and run:

> pip install -r requirements.txt
